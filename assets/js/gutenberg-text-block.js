const { registerBlockType } =   wp.blocks;
const blockStyle = {

};

registerBlockType('ttt_theme/custom-gutenberg-text-block',{
   title: 'Custom Text Block',
    description: 'This is the custom gutenberg text block',
    category: 'common',
    icon: 'smiley',
    attributes:{
       content:{type:'string'},
        color : {type: 'string'},

    },
    edit: function (props) {
        return wp.element.createElement(
            "div",
            null,
            wp.element.createElement(
                "h3",
                null,
                "Cool Border Box",

            ),
            wp.element.createElement("input",{type:"text"})
        );
    },
    save: function (props) {
        return null;
    }
});
