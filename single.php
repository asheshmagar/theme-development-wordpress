<?php get_header(); ?>
	<div class="content-container">
		<div class="main-content">
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) :
					the_post(); ?>
					<article class="post">
						<h3>
							<?php the_title(); ?>
						</h3>
						<div class="meta">
							Created By: <strong><?php the_author(); ?> </strong>&nbsp on
							&nbsp <?php the_time( 'F j, Y' ); ?>
						</div><!--/.meta-->
						<?php if ( has_post_thumbnail() ) : ?>
							<div class="post-thumbnail">
								<?php the_post_thumbnail(); ?>
							</div> <!--/.post-thumbnail-->
						<?php endif; ?>
						<br />
						<p><?php the_content(); ?></p>
					</article><!--/.post-->
				<?php endwhile; ?>
			<?php else : ?>
				<?php echo wpautop( "Sorry, no posts were found!" ); ?>
			<?php endif; ?>
		</div><!--/.main-content-->
		<div class="side-bar">
			<?php if ( is_active_sidebar( 'sidebar' ) ) : ?>
				<?php dynamic_sidebar( 'sidebar' ); ?>
			<?php endif; ?>
		</div><!--/.side-bar-->
	</div><!--/.content-container-->
<?php get_footer(); ?>

