/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./app/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app/gutenberg-block/index.js":
/*!**************************************!*\
  !*** ./app/gutenberg-block/index.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var registerBlockType = wp.blocks.registerBlockType;\nvar __ = wp.i18n.__;\nwp.blocks.registerBlockType(\"gutenberg-block/custom-block\", {\n  title: __(\"Custom Block\", \"custom block\"),\n  description: __(\"Provides a short summary\", \"custom block\"),\n  //Common,formatting,layouts,widgets,embed\n  category: \"common\",\n  icon: \"welcome-learn-more\",\n  keyword: [__(\"Text\", \"text\"), __(\"Custom\", \"custom\")],\n  supports: {\n    html: false\n  },\n  edit: function edit(props) {\n    return wp.element.createElement(\"div\", {\n      className: props.className\n    }, wp.element.createElement(\"ul\", {\n      class: \"list-unstyled\"\n    }, wp.element.createElement(\"li\", null, wp.element.createElement(\"strong\", null, __(\"Text\", \"custom\"), \": \"), \" TEXT_PH\"), wp.element.createElement(\"li\", null, wp.element.createElement(\"strong\", null, __(\"Color\", \"custom\"), \" \"), \" COLOR_PH\"), wp.element.createElement(\"li\", null, wp.element.createElement(\"strong\", null, __(\"Broder\", \"custom\")), \" BORDER_PH\"), wp.element.createElement(\"li\", null, wp.element.createElement(\"strong\", null, __(\"Font Style\", \"custom\")), \" FONT_STYLE_PH\"), wp.element.createElement(\"li\", null, wp.element.createElement(\"strong\", null, __(\"Font Weight\", \"custom\")), \" FONT_WEIGHT_PH\")));\n  },\n  save: function save(props) {\n    return wp.element.createElement(\"p\", null, \"Hello world \");\n  }\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9hcHAvZ3V0ZW5iZXJnLWJsb2NrL2luZGV4LmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vYXBwL2d1dGVuYmVyZy1ibG9jay9pbmRleC5qcz83ZTdhIl0sInNvdXJjZXNDb250ZW50IjpbInZhciByZWdpc3RlckJsb2NrVHlwZSA9IHdwLmJsb2Nrcy5yZWdpc3RlckJsb2NrVHlwZTtcbnZhciBfXyA9IHdwLmkxOG4uX187XG53cC5ibG9ja3MucmVnaXN0ZXJCbG9ja1R5cGUoXCJndXRlbmJlcmctYmxvY2svY3VzdG9tLWJsb2NrXCIsIHtcbiAgdGl0bGU6IF9fKFwiQ3VzdG9tIEJsb2NrXCIsIFwiY3VzdG9tIGJsb2NrXCIpLFxuICBkZXNjcmlwdGlvbjogX18oXCJQcm92aWRlcyBhIHNob3J0IHN1bW1hcnlcIiwgXCJjdXN0b20gYmxvY2tcIiksXG4gIC8vQ29tbW9uLGZvcm1hdHRpbmcsbGF5b3V0cyx3aWRnZXRzLGVtYmVkXG4gIGNhdGVnb3J5OiBcImNvbW1vblwiLFxuICBpY29uOiBcIndlbGNvbWUtbGVhcm4tbW9yZVwiLFxuICBrZXl3b3JkOiBbX18oXCJUZXh0XCIsIFwidGV4dFwiKSwgX18oXCJDdXN0b21cIiwgXCJjdXN0b21cIildLFxuICBzdXBwb3J0czoge1xuICAgIGh0bWw6IGZhbHNlXG4gIH0sXG4gIGVkaXQ6IGZ1bmN0aW9uIGVkaXQocHJvcHMpIHtcbiAgICByZXR1cm4gd3AuZWxlbWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtcbiAgICAgIGNsYXNzTmFtZTogcHJvcHMuY2xhc3NOYW1lXG4gICAgfSwgd3AuZWxlbWVudC5jcmVhdGVFbGVtZW50KFwidWxcIiwge1xuICAgICAgY2xhc3M6IFwibGlzdC11bnN0eWxlZFwiXG4gICAgfSwgd3AuZWxlbWVudC5jcmVhdGVFbGVtZW50KFwibGlcIiwgbnVsbCwgd3AuZWxlbWVudC5jcmVhdGVFbGVtZW50KFwic3Ryb25nXCIsIG51bGwsIF9fKFwiVGV4dFwiLCBcImN1c3RvbVwiKSwgXCI6IFwiKSwgXCIgVEVYVF9QSFwiKSwgd3AuZWxlbWVudC5jcmVhdGVFbGVtZW50KFwibGlcIiwgbnVsbCwgd3AuZWxlbWVudC5jcmVhdGVFbGVtZW50KFwic3Ryb25nXCIsIG51bGwsIF9fKFwiQ29sb3JcIiwgXCJjdXN0b21cIiksIFwiIFwiKSwgXCIgQ09MT1JfUEhcIiksIHdwLmVsZW1lbnQuY3JlYXRlRWxlbWVudChcImxpXCIsIG51bGwsIHdwLmVsZW1lbnQuY3JlYXRlRWxlbWVudChcInN0cm9uZ1wiLCBudWxsLCBfXyhcIkJyb2RlclwiLCBcImN1c3RvbVwiKSksIFwiIEJPUkRFUl9QSFwiKSwgd3AuZWxlbWVudC5jcmVhdGVFbGVtZW50KFwibGlcIiwgbnVsbCwgd3AuZWxlbWVudC5jcmVhdGVFbGVtZW50KFwic3Ryb25nXCIsIG51bGwsIF9fKFwiRm9udCBTdHlsZVwiLCBcImN1c3RvbVwiKSksIFwiIEZPTlRfU1RZTEVfUEhcIiksIHdwLmVsZW1lbnQuY3JlYXRlRWxlbWVudChcImxpXCIsIG51bGwsIHdwLmVsZW1lbnQuY3JlYXRlRWxlbWVudChcInN0cm9uZ1wiLCBudWxsLCBfXyhcIkZvbnQgV2VpZ2h0XCIsIFwiY3VzdG9tXCIpKSwgXCIgRk9OVF9XRUlHSFRfUEhcIikpKTtcbiAgfSxcbiAgc2F2ZTogZnVuY3Rpb24gc2F2ZShwcm9wcykge1xuICAgIHJldHVybiB3cC5lbGVtZW50LmNyZWF0ZUVsZW1lbnQoXCJwXCIsIG51bGwsIFwiSGVsbG8gd29ybGQgXCIpO1xuICB9XG59KTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./app/gutenberg-block/index.js\n");

/***/ }),

/***/ "./app/index.js":
/*!**********************!*\
  !*** ./app/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _gutenberg_block_index_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./gutenberg-block/index.js */ \"./app/gutenberg-block/index.js\");\n/* harmony import */ var _gutenberg_block_index_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_gutenberg_block_index_js__WEBPACK_IMPORTED_MODULE_0__);\n//Main File\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9hcHAvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hcHAvaW5kZXguanM/ZTkyNSJdLCJzb3VyY2VzQ29udGVudCI6WyIvL01haW4gRmlsZVxuaW1wb3J0ICcuL2d1dGVuYmVyZy1ibG9jay9pbmRleC5qcyc7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTsiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./app/index.js\n");

/***/ })

/******/ });