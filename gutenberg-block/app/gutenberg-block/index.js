const { registerBlockType } = wp.blocks;
const { __ } = wp.i18n;

wp.blocks.registerBlockType("gutenberg-block/custom-block", {
  title: __("Custom Block", "custom block"),
  description: __("Provides a short summary", "custom block"),

  //Common,formatting,layouts,widgets,embed
  category: "common",
  icon: "smiley",
  keyword: [__("Text", "text"), __("Custom", "custom")],
  supports: {
    html: false,
  },
 /* edit: (props) => {
    return (
      <div className={ props.className }>
        <ul class="list-unstyled">
          <li>
            <strong>{__("Text", "custom")}: </strong> TEXT_PH
          </li>
          <li>
            <strong>{__("Color", "custom")} </strong> COLOR_PH
          </li>
          <li>
            <strong>{__("Broder", "custom")}</strong> BORDER_PH
          </li>
          <li>
            <strong>{__("Font Style", "custom")}</strong> FONT_STYLE_PH
          </li>
          <li>
            <strong>{__("Font Weight", "custom")}</strong> FONT_WEIGHT_PH
          </li>
        </ul>
      </div>
    );
  },
  save: (props) => {
    return <p>Hello world </p>;
  },*/
});
