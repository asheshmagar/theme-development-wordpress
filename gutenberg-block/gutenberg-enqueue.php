<?php


function r_enqueue_block_editor_assets() {
	wp_register_script(
		'r_blocks_bundle',
		get_theme_file_uri() . '/gutenberg-block/dist/bundle.js',
		[ 'wp-i18n', 'wp-element', 'wp-blocks', 'wp-components', 'wp-editor', 'wp-api' ],
		fileatime( get_theme_file_uri() . '/gutenberg-block/dist/bundle.js' )
	);

	wp_enqueue_script( 'r_blocks_bundle' );
}