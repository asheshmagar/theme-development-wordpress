<?php

/**
 *Adds TTT_Text_Widget
 */
class TTT_Text_Widget extends WP_Widget {

	/**
	 * Before and after arguments like '<div>' & '</div>'
	 */
	public $args = array(
		'before_title'  => '<h3 class="ttt-widget-title">',
		'after-title'   => '</h3>',
		'before-widget' => '<div class="ttt-widget-wrap">',
		'after-widget'  => '</div>',
	);

	//Register widget  with WordPress
	public function __construct() {
		parent::__construct(
			'ttt-text', //Base ID
			esc_html__( 'TTT Text', 'ttt_text_domain' ), //Name
			array( 'description' => esc_html__( 'A simple text widget.', 'ttt_text_domain' ) ) //Args

		);
		add_action( 'widgets_init', function () {
			register_widget( 'TTT_Text_Widget' );
		} );
	}

	/**
	 * Front-end display of widget
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		echo esc_html__( $instance['text'], 'ttt_text_domain' );

		echo $args['after_widget'];
	}

	/**
	 * Back-end display of widget form
	 *
	 * @param $instance
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( '', 'ttt_text_domain' );
		$text  = ! empty( $instance['text'] ) ? $instance['text'] : esc_html__( '', 'ttt_text_domain' );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ) ?>">
				<?php echo esc_html__( 'Title : ', 'ttt_text_domain' ) ?>
			</label>
			<input for="<?php echo esc_attr( $this->get_field_id( 'title' ) ) ?>"
			       name="<?php echo esc_attr( $this->get_field_name( 'title' ) ) ?>"
			       type="text" value="<?php echo esc_attr( $title ); ?>"
			/>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'text' ) ) ?>">
				<?php echo esc_html__( 'Description : ', 'ttt_text_domain' ); ?>
			</label>
			<textarea name="<?php echo esc_attr( $this->get_field_name( 'text' ) ) ?>"
			          id="<?php echo esc_attr( $this->get_field_id( 'text' ) ) ?>" cols="30" rows="10"
			          value="<?php echo esc_attr( $text ) ?>"
			>
            </textarea>
		</p>


		<?php
	}

	/**
	 * Updates a particular instance of a widget.
	 *
	 * This function should check that `$new_instance` is set correctly. The newly-calculated
	 * value of `$instance` should be returned. If false is returned, the instance won't be
	 * saved/updated.
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 *
	 * @return array Settings to save or bool false to cancel saving.
	 * @since 2.8.0
	 *
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();

		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['text']  = ( ! empty( $new_instance['text'] ) ) ? strip_tags( $new_instance['text'] ) : '';

		return $instance;
	}
} //class TTT_Text_Widget

$custom_text_widget = new TTT_Text_Widget();
