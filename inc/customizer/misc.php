<?php
function ttt_misc_customizer_section($wp_customize){
	$wp_customize->add_setting('ttt_header_show_search',[
		'default'=>'yes',
	]);

	$wp_customize->add_setting('ttt_footer_copyright_text',[
		'default'=>'yes',
	]);

	$wp_customize->add_setting('ttt_footer_toc_page',[
		'default'=>0,
	]);

	$wp_customize->add_setting('ttt_footer_privacy_page',[
		'default'=>0,
	]);

	$wp_customize->add_section('ttt_misc_section',[
		'title' => __('Twenty Twenty Three Misc Settings','ttt'),
		'priority'=>30
	]);

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'ttt_header_show_search',
		array(
			'label' => __('Show Search Button in Header','ttt'),
			'section' => 'ttt_misc_section',
			'settings' => 'ttt_header_show_search',
			'type' => 'checkbox',
			'choices' => [
				'yes' => 'Yes'
			]
		)
	));

}