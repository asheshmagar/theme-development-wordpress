<?php

function ttt_social_customizer_section( $wp_customize ) {
	$wp_customize->add_setting( 'ttt_facebook_handle', [
		'default' => '',
	] );

	$wp_customize->add_setting( 'ttt_twitter_handle', [
		'default' => '',
	] );

	$wp_customize->add_setting( 'ttt_linkedin_handle', [
		'default' => '',
	] );

	$wp_customize->add_setting( 'ttt_instagram_handle', [
		'default' => '',
	] );

	$wp_customize->add_section( 'ttt_social_section', [
		'title'    => __( 'Twenty Twenty Three Social Settings', 'ttt' ),
		'priority' => 30
	] );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize,
		'ttt_social_facebook_input',
		array(
			'label'    => __( 'Facebook Handle', 'ttt' ),
			'section'  => 'ttt_social_section',
			'settings' => 'ttt_facebook_handle'
		)
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize,
		'ttt_social_twitter_input',
		array(
			'label'    => __( 'Twitter Handle', 'ttt' ),
			'section'  => 'ttt_social_section',
			'settings' => 'ttt_twitter_handle'
		)
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize,
		'ttt_social_linkedin_input',
		array(
			'label'    => __( 'LinkedIn Handle', 'ttt' ),
			'section'  => 'ttt_social_section',
			'settings' => 'ttt_linkedin_handle'
		)
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize,
		'ttt_social_instagram_input',
		array(
			'label'    => __( 'Instagram Handle', 'ttt' ),
			'section'  => 'ttt_social_section',
			'settings' => 'ttt_instagram_handle'
		)
	) );
}