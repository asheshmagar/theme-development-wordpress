<?php
function ttt_customize_register( $wp_customize ) {
	ttt_social_customizer_section( $wp_customize );
	ttt_misc_customizer_section( $wp_customize );
}