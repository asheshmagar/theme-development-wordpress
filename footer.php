<footer id="site-footer" class="main-footer">
    <span>&copy; </span> &nbsp; <?php echo date('Y'); ?> &nbsp; <span><?php bloginfo('name')?></span>
</footer> <!-- /#site-footer -->
<?php wp_footer(); ?>
</body>

</html>