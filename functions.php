<?php
/**
 * Enqueue styles and scripts
 */
function twentytwentythree_register_scripts() {

	//Variable to  hold get_theme_file_uri()
	$uri = get_theme_file_uri();

	//Enqueue Font Awesome CSS library
	wp_enqueue_style('ttt-font-awesome','https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css');

	//Enqueue Google Fonts
	wp_enqueue_style( 'ttt-google-font', "https://fonts.googleapis.com/css2?family=Roboto+Mono&display=swap" );

	//Enqueue Main Style Sheet
	wp_enqueue_style( 'twentytwentythree-style', get_stylesheet_uri(), [ 'ttt-google-font','ttt-font-awesome' ], time() );

	//Enqueue the main JS Script in footer
	wp_enqueue_script( 'twentytwentythree-script', $uri . '/assets/js/script.js', [], time(), true );
}


//Theme setup
function twentytwentythree_theme_setup() {
	//Display posts' featured image to front
	add_theme_support( 'post-thumbnails' );
}


//Specify Widget Location
function sidebar_widgets() {
	register_sidebar( array(
		'name' => 'Sidebar',
		'id'   => 'sidebar'
	) );
}


//Set Excerpt Length
function twentytwentythree_excerpt_length() {
	return 20;
}

//Define Constants
define( 'TTT_PARENT_DIR', get_template_directory() );
define( 'TTT_PARENT_INC_DIR', TTT_PARENT_DIR . '/inc' );

//Include Files
require TTT_PARENT_INC_DIR . '/text-widget.php';
require TTT_PARENT_INC_DIR . '/ttt-customizer.php';
require TTT_PARENT_INC_DIR . '/customizer/social.php';
require TTT_PARENT_INC_DIR . '/customizer/misc.php';
require TTT_PARENT_DIR . '/gutenberg-block/gutenberg-enqueue.php';



//Hooks
add_action( 'wp_enqueue_scripts', 'twentytwentythree_register_scripts' );

add_action( 'after_setup_theme', 'twentytwentythree_theme_setup' );

add_action( 'widgets_init', 'sidebar_widgets' );

add_filter( 'excerpt_length', 'twentytwentythree_excerpt_length' );

add_action( 'enqueue_block_editor_assets', 'r_enqueue_block_editor_assets' );

add_action('customize_register','ttt_customize_register');


