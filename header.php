<?php

/**
 * Header file.
 */

?>
<!DOCTYPE html>

<html class="no-js" lang="<?php language_attributes(); ?>">

<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="profile" href="https://gmpg.org/xfn/11">
    <title><?php bloginfo( 'name' ) ?></title>

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<?php wp_body_open(); ?>

<header id="site-header" class="main-header">
    <!-- Add code for site header here. -->
    <div class="site-identity">
        <h2>
			<?php bloginfo( 'title' ); ?>
        </h2>
        <p><?php bloginfo( 'description' ) ?></p>
    </div>
    <!--/.site-identity-->

    <div class="top-social">
        <ul>
			<?php
			if ( get_theme_mod( 'ttt_facebook_handle' ) ) {
				?>
                <li>
                    <a href="https://www.facebook.com/<?php echo get_theme_mod( 'ttt_facebook_handle' ); ?>">
                        <i class="fab fa-facebook-square fa-2x"></i>
                    </a>
                </li>
				<?php
			}
			if ( get_theme_mod( 'ttt_twitter_handle' ) ) {
				?>
                <li>
                    <a href="https://www.twitter.com/<?php echo get_theme_mod( 'ttt_facebook_handle' ); ?>">
                        <i class="fab fa-twitter-square fa-2x"></i>
                    </a>
                </li>
				<?php
			}
			if ( get_theme_mod( 'ttt_linkedin_handle' ) ) {
				?>
                <li>
                    <a href="https://www.linkedin.com/<?php echo get_theme_mod( 'ttt_facebook_handle' ); ?>">
                        <i class="fab fa-linkedin fa-2x"></i>
                    </a>
                </li>
				<?php
			}
			if ( get_theme_mod( 'ttt_instagram_handle' ) ) {
				?>
                <li>
                    <a href="https://www.instagram.com/<?php echo get_theme_mod( 'ttt_facebook_handle' ); ?>">
                        <i class="fab fa-instagram fa-2x"></i>
                    </a>
                </li>
				<?php
			}
			?>
        </ul>
    </div> <!--/.top-social-->
</header> <!-- /#site-header -->
<div class="search-bar">
    <form action="">
        <label for="s">
            <i class="fas fa-search"></i>
        </label>
        <input type="text" name="s">
    </form>
</div>